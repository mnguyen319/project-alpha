from tasks.models import Task
from django.forms import ModelForm


class Task(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "is_completed",
            "project",
            "assignee",
        ]
