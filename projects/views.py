from django.shortcuts import redirect

from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.detail import DetailView

from django.views.generic.edit import CreateView

from projects.models import Project


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list.html"
    context_object_name = "projects"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/detail.html"


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/new.html"
    fields = ["name", "description", "members"]

    # def form_valid(self, form):
    #     project = form.save(
    #         commit=False
    #     )  # setting up, but not actually saving
    #     project.save()
    #     project.members.add(self.request.user)
    #     project.save()
    #     return redirect("show_project", pk=project.id)
    def form_valid(self, form):
        project = form.save(commit=False)
        project.owner = self.request.user
        project.save()
        return redirect("show_project", pk=project.id)
